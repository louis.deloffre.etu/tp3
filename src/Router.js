export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];
	static link;

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		const elementClasses = document.querySelectorAll('.mainMenu a');
		if (this.link != null) {
			this.link.classList.remove('class', 'active');
		}
		elementClasses.forEach(element => {
			const lien = element.getAttribute('href');
			if (lien == path) {
				this.link = element;
				this.link.classList.add('class', 'active');
			}
		});
		console.log(elementClasses);

		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
		this.contentElement.classList.add('active');
	}
	static #menuElement;
	static set menuElement(element) {
		this.#menuElement = element;
		const lien = element.querySelectorAll('a');
		lien.forEach(element => {
			element.addEventListener('click', elem => {
				event.preventDefault();
				console.log(element.getAttribute('href'));
				Router.navigate(element.getAttribute('href'));
			});
		});
	}
}
