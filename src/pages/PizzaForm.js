import Page from './Page.js';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = document.querySelector('form');
		form.addEventListener('submit', event => {
			event.preventDefault();
			const input = form.querySelector('input[name=name]');
			if (input.value == '') {
				alert('Champ vide');
			} else {
				alert(`La pizza ${input.value} a été ajoutée`);
				input.value = '';
			}
			console.log(input.value);
		});
	}

	submit(event) {}
}
