import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';
import { doc } from 'prettier';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizza

const elem = document.querySelectorAll('.pizzaThumbnail h4');
console.log(elem[1]);
const logo = document.querySelector('.logo');
logo.innerHTML += `<small>les pizzas c'est la vie</small>`;

const urlFooter = document.querySelectorAll('footer div a');
console.log(urlFooter[2].getAttribute('href'));

const carte = document.querySelector('.pizzaListLink');
carte.setAttribute('class', 'active');

const display = document.querySelector('.newsContainer');
display.style = 'display:';

const buttonclose = document.querySelector('.closeButton');

function buttonClose(event) {
	event.preventDefault();
	const display = document.querySelector('.newsContainer');
	display.style = 'display:none';
}

buttonclose.addEventListener('click', buttonClose);

Router.menuElement = document.querySelector('.mainMenu');
